MESSAGE BLOCK MODULE

Disables theme default system messages variable and creates a block to show them. 
The block then can be positioned in any theme region as usual.

--------------------------------------------------------------------------------
BASED ON

Message module in: http://groups.drupal.org/node/51088
Which change usual behavior of displaying status messages from drupal_set_message() 

--------------------------------------------------------------------------------
INSTALL

1 - Download, unzip/untar and activate the module. 
(More info about installing contributed modules: 
http://drupal.org/documentation/install/modules-themes/modules-7)

2 - Put the block within desired theme region in admin/structure/blocks 

--------------------------------------------------------------------------------
TODO

Allow disable $messages variable by theme
--------------------------------------------------------------------------------
